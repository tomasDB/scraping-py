from typing import Text
from bs4 import BeautifulSoup
from selenium import webdriver
import time

start_code = time.time()
#DriverConfig
edge_driver = webdriver.Edge(r'RutaUbicacionDriver')

### Lista de componentes de PC ###
list_category = [ 'Motherboards/CAT=47/OR=1/' , 'Memorias-Para-PC/CAT=50/OR=1/page.page'
                , 'Microprocesador/CAT=52/OR=1/page.page' , 'Discos-Internos-HDD-Para-PC/CAT=53/OR=1/page.page'
                , 'Discos-Solidos-SSD-Internos/CAT=53/OR=1/page.page' , 'Gabinetes/CAT=1/OR=1/page.page'
                , 'Fuentes/CAT=7' , 'Refrigeracion/CAT=2/OR=1/page.page' , 'Placas-de-Video/CAT=48/OR=1/page.page'
                , 'Cable-y-Adaptadores/CAT=5/OR=1/page.page' , 'Monitores/CAT=59/OR=1/page.page'
                , 'PC-Basico/OR=1/wco=61/tv=/page.page' , 'PC-Workstation/OR=1/wco=62/tv=/page.page'
                , 'PC-Gamer/OR=1/wco=63/tv=/page.page' , 'Mini-PC/CAT=68/SCAT=3999/OR=1/page.page']
#Creamos una lista donde se almacenarán los productos
lista_productos = []

for sub_category_search in list_category:
    #Seteamos la variable en False para que pueda entrar al ciclo
    change_category = False

    #Entramos a la página 1
    home_link = 'https://Link.link.com/Productos/{}'.format(sub_category_search)
    edge_driver.get(home_link)

    while not change_category:
        time.sleep(2)
        #Cargamos el HTML en BeautifulSoup
        page = BeautifulSoup(edge_driver.page_source,'html.parser')
        
        #Recorremos los elementos de la página 'x'  
        for product in page.findAll('div', attrs={'class':'product'}):
            
            #La página no muestra los productos fuera de stock
            title = product.find('a', attrs={'class':'titprod'}).text
            link = product.find('a', attrs={'class':'titprod'}).get('href')
            link_photo = product.find('img' , attrs={'class':'img-responsive'}).get('src')
            price = product.find('div', attrs={'class':'price'}).text 

            #Guardando el Productos en un diccionario
            dic_productos = {}
            dic_productos['title_product'] = title
            dic_productos['link_product'] = link
            dic_productos['link_photo'] = link_photo
            dic_productos['price_product'] = price
            lista_productos.append(dic_productos)

        
        #Controlamos si la pagina 'x' es la ultima

        flag_ultima_page = page.find('li' , attrs={'class':'button-pager'})

        #Si no existe el botón con la búsqueda especificada es porque no hay otra página para obtener datos
        #caso contrario simplemente buscamos el botón y lo clickeamos
        if not flag_ultima_page:
            change_category = True
        else:
            next_btn =  edge_driver.find_element_by_xpath(".//li[contains(@class, 'button-pager')]")
            next_btn.click()

#Cerramos el driver de búsqueda
edge_driver.close()
final_code = time.time()

#Simplemente para revisar el tiempo en segundos que nos demoramos en realizar el scraping
print(final_code - start_code)