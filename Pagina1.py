from typing import Text
from bs4 import BeautifulSoup
from selenium import webdriver
import time

start_code = time.time()
#DriverConfig
edge_driver = webdriver.Edge(r'RutaUbicacionDriver')


### Lista de componentes de PC ###
list_category = [ 'motherboards' , 'discos-duros-mecanicos' , 'discos-solidos-ssd' , 'memorias-ram' 
                , 'microprocesadores' , 'placas-de-video' , 'gabinetes' , 'placas-de-sonido'
                , 'fuentes' , 'placas-de-red' , 'refrigeracion' , 'combos-de-actualizacion'
                , 'pcs-de-escritorio' , 'servidores' , 'mini-pcs' , 'teclados' 
                , 'gaming-kit' , 'monitores']

#Creamos una lista donde se almacenarán los productos
lista_productos = []

for sub_category_search in list_category:

    #Seteamos la variable en False para que pueda entrar al ciclo
    change_category = False

    #Entramos a la pagina 1
    home_link = "https://link.link.com/"+sub_category_search
    edge_driver.get(home_link)

    while not change_category:
        time.sleep(2)
       #Cargamos el HTML en BeautifulSoup
        page = BeautifulSoup(edge_driver.page_source,'html.parser')

        #Recorremos los elementos de la página 'x'
        for product in page.findAll('div', attrs={'class':'col-container col-xs-12 col-md-8 col-lg-8'}):
            
            #Consultamos si el producto tiene Stock
            stock = product.find('button', attrs={'class':'btn btn-no-stock btn-add-product'})
            if not stock:
                #Si hay stock obtenemos los datos del mismo
                title = product.find('h3', attrs={'class':'product-box-title'}).text
                link = product.find('a', attrs={'class':'product-box-price clearfix'}).get('href')
                link_photo = 'https://link.link.com/' +  product.find('img' , attrs={'class':'img-contained'}).get('src')
                price = product.find('span', attrs={'class':'current-price'}).text 

                #Guardando el Productos en un diccionario
                dic_productos = {}
                dic_productos['title_product'] = title
                dic_productos['link_product'] = link
                dic_productos['link_photo'] = link_photo
                dic_productos['price_product'] = price
                lista_productos.append(dic_productos)
            else:
                #Si el producto no tiene stock no buscamos más dentro de la page x
                change_category = True

        ### Saltamos a la siguiente página ###

        #Controlamos que exista otra página
        results = page.findAll('i', attrs={'class':'fa fa-chevron-right'})
        flag_sig = False

        #Controlamos que el botón exista para ello:
        #Recorremos los 'X' elementos que encontremos con page.findAll('i', attrs={'class':'fa fa-chevron-right'})
        for result in results:

            #Luego de estudiar la página el botón siguiente solamente tendrá un atributo el resto de los elementos
            #poseen más de uno por lo que lo identificamos y seteamos la variable flag_sig a True
            if len(result.attrs) == 1:
                flag_sig = True

        #Si hay otra pagina realizamos los procedimientos de control para cambiar
        if flag_sig:
            
            try:
                if not change_category:
                    btns = edge_driver.find_elements_by_xpath(".//a[contains(@title, ' Anterior ')]")
                    if len(btns) > 0:
                        next_btn = btns[-1]
                        next_btn.click()
                else:
                    change_category = True 
            except:
                pass
        else:
            change_category = True

#Cerramos el driver de búsqueda
edge_driver.close()
final_code = time.time()

#Simplemente para revisar el tiempo en segundos que nos demoramos en realizar el scraping
print(final_code - start_code)

"""
Html de ejemplificación

    <ul class="pagination">
    <li><a href="https://link.link.com/categoria?cPath=234&amp;page=1" class="pageResults" title="Anterior"><i
                class="fa fa-chevron-left"></i></a></li>
    <li><a href="https://link.link.com/categoria?cPath=234&amp;page=1" class="pageResults" title="Pagina 1">1</a>
    </li>
    <li class="active"><span><strong>2</strong></span></li>
    <li><a href="https://link.link.com/categoria?cPath=234&amp;page=3" class="pageResults" title="Pagina 3">3</a>
    </li>
    <li><a href="https://link.link.com/categoria?cPath=234&amp;page=4" class="pageResults" title="Pagina 4">4</a>
    </li>
    <li><a href="https://link.link.com/categoria?cPath=234&amp;page=5" class="pageResults" title="Pagina 5">5</a>
    </li>
    <li><a href="https://link.link.com/categoria?cPath=234&amp;page=6" class="pageResults"
            title="Siguientes 5 paginas">...</a></li>
    <li><a href="https://link.link.com/categoria?cPath=234&amp;page=3" class="pageResults" title="Anterior"><i
                class="fa fa-chevron-right"></i></a></li>
</ul>

Problema encontrado:
    -   No me fue posible identificar el botón para avanzar a la próxima lista de productos dentro de la misma categoria,
        ya que el driver.find_elements_by_xpath() me "confundia" los botones de siguiente y anterior lo que provocaba que
        quede en un bucle la ejecución.
 
Solución propuesta:
    -   Para evitar el problema mencionado
        1. Controlamos que haya una proxima pagina de la cual extraer información analizando la existencia del botón con
        el fragmento <i class="fa fa-chevron-right"></i>.
        2. Una ves controlado buscamos los elementos con la funcion driver.find_elements_by_xpath() la cual nos va a traer
        tanto el botón de siguiente como el de anterior.
        3. Seleccionamos el botón ultimo que sera el de siguiente para acceder a la proxima pagina.
"""